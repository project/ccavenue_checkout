/**
 * @file
 * CCAvenue Checkout custom js code.
 */

(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.ccavenue_checkout = {
    attach: function (context, settings) {
      // Submit the payment process form
      $('#ccavenueform', context).once('ccavenue_checkout').delay(1000).submit();
    }
  };

})(jQuery, Drupal);