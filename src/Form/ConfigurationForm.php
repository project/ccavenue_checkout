<?php

namespace Drupal\ccavenue_checkout\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Class ConfigurationForm.
 */
class ConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ccavenue_checkout.configuration',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ccavenue_admin_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('ccavenue_checkout.configuration');
    $checkout_modes = ['test' => 'Testing', 'live' => 'Live'];
    $form['checkout_mode'] = [
      '#type' => 'select',
      '#title' => $this->t('Checkout Mode'),
      '#description' => $this->t('Select checkout environment mode'),
      '#options' => $checkout_modes,
      '#default_value' => $config->get('checkout_mode'),
      '#required' => TRUE,
    ];
    $form['merchant_id'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Merchant Id'),
      '#description' => $this->t('Merchant Id of payment gateway account'),
      '#default_value' => $config->get('merchant_id'),
      '#required' => TRUE,
    ];
    $form['access_code'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Access Code'),
      '#description' => $this->t('Access Code of payment gateway account'),
      '#default_value' => $config->get('access_code'),
      '#required' => TRUE,
    ];
    $form['working_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Working Key'),
      '#description' => $this->t('Working Key of payment gateway account'),
      '#default_value' => $config->get('working_key'),
      '#required' => TRUE,
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('ccavenue_checkout.configuration')
      ->set('checkout_mode', $form_state->getValue('checkout_mode'))
      ->set('merchant_id', $form_state->getValue('merchant_id'))
      ->set('access_code', $form_state->getValue('access_code'))
      ->set('working_key', $form_state->getValue('working_key'))
      ->save();
  }

}
