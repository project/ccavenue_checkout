<?php

namespace Drupal\ccavenue_checkout\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;

/**
 * Implements the PaymentForm.
 */
class PaymentForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ccavenue_payment_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $current_path = \Drupal::service('path.current')->getPath();
    $path_array = explode('/', $current_path);
    $node_data = entity_load('node', $path_array[2]);

    $form['productinfo'] =  [
      '#type' => 'hidden',
      '#value' => [
        'title' => $node_data->get('title')->getValue()[0]['value'],
      ]
    ];
    
    $form['returnurl'] = [
      '#type' => 'hidden',
      '#value' => $current_path
    ];

    $form['udf1'] = [
      '#type' => 'hidden',
      '#value' => $node_data->get('nid')->getValue()[0]['value']
    ];
    
    $form['#attributes']['class'][] = 'custom-form';
    
    $form['billing_name'] = [
      '#title' => $this->t('NAME'),
      '#type' => 'textfield',
      '#required' => TRUE,
    ];
    
    $form['billing_email'] = [
      '#title' => $this->t('EMAIL'),
      '#type' => 'email',
      '#required' => TRUE,
    ];
    
    $form['billing_mobile'] = [
      '#title' => $this->t('MOBILE'),
      '#type' => 'tel',
      '#required' => TRUE,
      '#pattern' => '[1-9][0-9]{9}',
    ];
    
    $form['billing_address_1'] = [
      '#title' => $this->t('ADDRESS 1'),
      '#type' => 'textfield',
      '#required' => TRUE,
    ];
    
    $form['billing_address_2'] = [
      '#title' => $this->t('ADDRESS 2'),
      '#type' => 'textfield',
    ];
    
    $form['billing_city'] = [
      '#title' => $this->t('CITY'),
      '#type' => 'textfield',
      '#required' => TRUE,
    ];
    
    $form['billing_state'] = [
      '#title' => $this->t('STATE'),
      '#type' => 'textfield',
      '#required' => TRUE,
    ];
    
    $form['billing_country'] = [
      '#title' => $this->t('COUNTRY'),
      '#type' => 'textfield',
      '#required' => TRUE,
    ];
    
    $form['billing_zip'] = [
      '#title' => $this->t('ZIPCODE'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#pattern' => '[1-9][0-9]{5}',
    ];
    
    $form['submit'] = [
      '#value' => 'PROCEED TO PAYMENT',
      '#type' => 'submit',
      '#attributes'=>[
        'class'=> ['payment_button'],
      ],
    ];
    
    $form['#action']= '/process_payment';
    
    return $form;

  }  
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

  }

}
