<?php

namespace Drupal\ccavenue_checkout\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use \Drupal\node\Entity\Node;
use Drupal\Core\Url;
use Drupal\ccavenue_checkout\CCAvenueEncryption;

/**
 * Implements the SubmitForm.
 */
class SubmitForm extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ccavenueForm';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    // Get Payment default settings
    $cur = \Drupal::config('ccavenue_checkout.settings')->get('default_currency');
    $lng = \Drupal::config('ccavenue_checkout.settings')->get('default_language');

    // Get Admin CCAvenue Config settings
    $config = \Drupal::config('ccavenue_checkout.configuration');
    $checkout_mode = $config->get('checkout_mode');
    $merchant_id = $config->get('merchant_id');
    $access_code = $config->get('access_code');
    $working_key = $config->get('working_key');

    $hdfc_api_test_url = 'https://test.ccavenue.com/transaction/transaction.do?command=initiateTransaction';
    $hdfc_api_url = 'https://secure.ccavenue.com/transaction/transaction.do?command=initiateTransaction';

    if ($checkout_mode == 'test') {
      $action_url = $hdfc_api_test_url;
    }
    else {
      $action_url = $hdfc_api_url;
    }

    $order_id = time();
    $posted = [];
    $posted['udf1'] = \Drupal::request()->request->get('udf1'); // node id
    $node = Node::load($posted['udf1']);
    $amount = $node->get('field_price')->getString();

    $posted['return_url'] = \Drupal::request()->request->get('returnurl');
    $posted['productinfo'] = \Drupal::request()->request->get('productinfo');
    $posted['billing_name'] = \Drupal::request()->request->get('billing_name');
    $posted['billing_email'] = \Drupal::request()->request->get('billing_email');
    $posted['billing_mobile'] = \Drupal::request()->request->get('billing_mobile');
    $posted['billing_address_1'] = \Drupal::request()->request->get('billing_address_1');
    $posted['billing_address_2'] = \Drupal::request()->request->get('billing_address_2');
    $posted['billing_city'] = \Drupal::request()->request->get('billing_city');
    $posted['billing_state'] = \Drupal::request()->request->get('billing_state');
    $posted['billing_country'] = \Drupal::request()->request->get('billing_country');
    $posted['billing_zip'] = \Drupal::request()->request->get('billing_zip');
    
    $redirect_url = Url::FromRoute('ccavenue_checkout.payment_status', ['commerce_order' => $order_id, 'step' => 'payment'], ['absolute' => TRUE])->toString();

    $parameters = [
      'merchant_id' => $merchant_id,
      'order_id' => $order_id,
      'amount' => $amount,
      'currency' => $cur,
      'language' => $lng,
      'redirect_url' => $redirect_url,
      'billing_name' => $posted['billing_name'],
      'billing_address' => $posted['billing_address_1'] . ' ' .$posted['billing_address_2'],
      'billing_city' => $posted['billing_city'],
      'billing_state' => $posted['billing_state'],
      'billing_country' => $posted['billing_country'],
      'billing_zip' => $posted['billing_zip'],
      'billing_email' => $posted['billing_email'],
      'billing_tel' => $posted['billing_mobile'],
      'merchant_param1' => $posted['udf1'],
      'merchant_param2' => $posted['productinfo'],
      'merchant_param3' => $posted['return_url'],
    ];

    // Encrypt the post data
    $encrypt = new CCAvenueEncryption();
    $merchantData = '';
    foreach ($parameters as $key => $value) {
      $merchantData .= $key . '=' . $value . '&';
    }
    $encrypted_data = $encrypt->encrypt($merchantData, $working_key);
    
    $form['access_code'] = [
      '#type' => 'hidden',
      '#title' => 'access_code',
      '#value' => $access_code,
    ]; 
    
    $form['encRequest'] = [
      '#type' => 'hidden',
      '#title' => 'encRequest',
      '#value' => $encrypted_data,
    ];
    
    $form['#action'] = $action_url;
    
    return $form;

  }  
  
  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    return;
  }

}
