<?php

namespace Drupal\ccavenue_checkout\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Url;

/**
 * Provides a 'CCAvenue Payment Form Block' block.
 *
 * @Block(
 *   id = "ccavenue_payment_form_block",
 *   admin_label = @Translation("CCAvenue Payment Form Block"),
 * )
 */

class PaymentFormBlock extends BlockBase {

  /**
   * {@inheritdoc}
   */
  public function build() {
    $form = \Drupal::formBuilder()->getForm('\Drupal\ccavenue_checkout\Form\PaymentForm');
    return $form;
  }

}
