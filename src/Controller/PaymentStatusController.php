<?php

/**
 * @file
 * Contains \Drupal\ccavenue_checkout\Controller\PaymentStatusController.
 */

namespace Drupal\ccavenue_checkout\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use \Drupal\node\Entity\Node;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\ccavenue_checkout\CCAvenueEncryption;

class PaymentStatusController extends ControllerBase {

  /**
   * {@inheritdoc}
   */
  public function paymentStatus() {
    
    $request = \Drupal::request();
    $txnid = $request->get('orderNo');
    $order_status = "";
    $encResponse = $request->get('encResp');
    if (empty($encResponse)) {
      drupal_set_message($this->t('Security Error. Illegal access detected.'), 'error');
      throw new \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException();
    }
    // Get Admin CCAvenue Config settings
    $config = \Drupal::config('ccavenue_checkout.configuration');
    $working_key = $config->get('working_key');
    // Decrypt Response
    $decrypt = new CCAvenueEncryption();
    $rcvdString = $decrypt->decrypt($encResponse, $working_key);
    $decryptValues = explode('&', $rcvdString);
    $dataSize = sizeof($decryptValues);
    $order_status = '';
    $tracking_id = '';
    $bank_ref_no = '';
    $json_decrypt_vals = json_encode($decryptValues);
    if (!empty($decryptValues[1])) {
      $track_information = explode('=', $decryptValues[1]);
      $tracking_id = $track_information[1];
      $bank_information = explode('=', $decryptValues[2]);
      $bank_ref_no = $bank_information[1];
    }
    $decryptData = [];
    for ($i = 0; $i < $dataSize; $i++) {
      $information = explode('=', $decryptValues[$i]);
      $decryptData[$information[0]] = $information[1];
      if ($i == 3)
        $order_status = $information[1];
    }

    // Set retun url
    $return_url = Url::fromUri('internal:/');
    if (isset($decryptData['merchant_param3'])) {
      $destination = $decryptData['merchant_param3'];
      $return_url = Url::fromUri('internal:'.$destination);
    }
    $destination = $return_url->toString();

    // Set status messages and redirect
    switch ($order_status) {

      case 'Success':
        $output = t('The transaction is successfull with Transaction Id:' . ' ' . $txnid);
        drupal_set_message($output, 'status', TRUE);
        $response = new RedirectResponse($destination);
        return $response;
        break;

      case 'Aborted':
        $output = t('The transaction has been aborted with Transaction Id:' . ' ' . $txnid);
        drupal_set_message($output, 'status', TRUE);
        $response = new RedirectResponse($destination);
        return $response;
        break;

      case 'Failure':
        $output = t('The transaction is failed with Transaction Id:' . ' ' . $txnid);
        drupal_set_message($output, 'status', TRUE);
        $response = new RedirectResponse($destination);
        return $response;
        break;

      default:
        drupal_set_message($this->t('Security Error. Illegal access detected.'), 'error');
        $response = new RedirectResponse($destination);
        return $response;
        break;

    }

  }

}
